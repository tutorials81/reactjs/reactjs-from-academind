import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from'react-router-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'; //taken from React-Bootstrap website
import './App.css';
import { FavoritesContextProvider } from './store/favorites-context';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
    <FavoritesContextProvider>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </FavoritesContextProvider>
);


